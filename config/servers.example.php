<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

return [
    'exampleProjectName' =>  [
        'servers'   =>  [ // Connection via SSH. All field are required.
            'exampleServer' => [ // Your any server name
                'host'  =>  '',
                'port'  =>  '',
                'user'  =>  '',
                'pass'  =>  '',
                'dir'   =>  '' //path to application on server
            ],
//            'exampleDevServer'   =>  [
//                'host'  =>  '',
//                'port'  =>  '',
//                'user'  =>  '',
//                'pass'  =>  '',
//                'dir'   =>  ''
//            ],
//            'exampleTestServer'   =>  [
//                'host'  =>  '',
//                'port'  =>  '',
//                'user'  =>  '',
//                'pass'  =>  ''
//            ],
//            'exampleProdServer'  =>  [
//                'host'  =>  '',
//                'port'  =>  '',
//                'user'  =>  '',
//                'pass'  =>  '',
//                'dir'   =>  ''
//            ]
        ]
    ]
];