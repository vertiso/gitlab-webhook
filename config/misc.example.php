<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

return [
    'exampleProjectName' => [
        'misc' => [
            'gitlab_token' => '', // Your webhook token
            'log_dir' => getcwd() . '/var/log/exampleProjectLogDir', // Make sure, directory has write permission
        ]
    ]
];