<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

return [
    'exampleProjectName' => [
        'projects' => ['exampleRepositoryName'],
    ],
];