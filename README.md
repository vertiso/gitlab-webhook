#GitLab Webhook

Application to handle GitLab webhook callbacks. Deploy to dev, test and prod server.

### 1. Installation
Clone this repository and install dependencies.

### 2. Configuration
Modify files in /config and remove .example from name. 

Link need to have $_GET variable. Variable name is defined in /config/default.php and default value is **project**. Example link (https://example.com/?project=exampleProjectName).


