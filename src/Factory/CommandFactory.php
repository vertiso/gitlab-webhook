<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

namespace App\Factory;

use App\Command\CommandInterface;

class CommandFactory
{
    const COMMAND_CLASS_FORMAT = '\App\Command\%sCommand';

    /**
     * @param $commandName
     * @param $server
     * @param $branch
     * @param $requestData
     * @return CommandInterface
     * @throws \Exception
     */
    public static function getCommand($commandName, $server, $branch, $requestData): CommandInterface {
        $commandClassName = sprintf(self::COMMAND_CLASS_FORMAT, $commandName);
        if (!class_exists($commandClassName)) {
            throw new \Exception('Command class ' . $commandClassName . ' not exists!');
        }
        return new $commandClassName($server, $branch, $requestData);
    }
}