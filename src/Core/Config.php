<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

namespace App\Core;

class Config
{
    protected static $config = [];

    /**
     * @throws \Exception
     */
    public static function load() {
        $configDir = getcwd() . DIRECTORY_SEPARATOR . 'config';

        $config = require $configDir . DIRECTORY_SEPARATOR . 'default.php';
        $project = (isset($_GET[$config['config']['project_var']])) ?
            filter_var($_GET[$config['config']['project_var']], FILTER_SANITIZE_FULL_SPECIAL_CHARS) : null;

        if (!$project) {
            throw new \Exception('Project no defined!', 500);
        }

        $data = [];
        foreach (scandir($configDir) as $file) {
            if (strpos($file, '.') === 0 && substr($file, -4, 4) !== '.php' && $file == 'default.php') {
                continue;
            }

            $filePath = $configDir . DIRECTORY_SEPARATOR . $file;

            if (is_file($filePath) && is_readable($filePath)) {
                $cfg = require $filePath;

                if (isset($cfg[ $project ])) {
                    $data = array_replace_recursive($data, $cfg[$project]);
                }
            }
        }

        self::$config = array_replace_recursive($data, $config);
    }

    /**
     * @param $key
     * @return mixed
     * @throws \Exception
     */
    public static function get($key) {
        if (!isset(self::$config[$key])) {
            throw new \Exception('No defined key: ' . $key . ' in config!', 500);
        }

        return self::$config[$key];
    }
}