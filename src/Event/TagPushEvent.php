<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

namespace App\Event;

class TagPushEvent extends AbstractEvent
{

    protected function getEndpoint(): string
    {
        return 'prod';
    }

    protected function getEventName(): string
    {
        return 'tag_push';
    }
}