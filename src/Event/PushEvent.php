<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

namespace App\Event;

class PushEvent extends AbstractEvent
{
    protected function getEndpoint(): string
    {
        return explode('/', $this->data['ref'])[2];
    }

    protected function getEventName(): string
    {
        return 'push';
    }
}